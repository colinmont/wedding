$(document).ready(function () {
  //radio buttons
  $('.attending,.not-attending').hide();
  $('#restrictions').hide();
  $('#radio_yes').click(function () {
    $('.not-attending').hide('fast');
    $('.attending').show('fast');
  });
  $('#radio_no').click(function () {
    $('.attending').hide('fast');
    $('.not-attending').show('fast');
  });
  //side bar
  $(".button-collapse").sideNav();
  //select menu
   $('select').material_select();
    //scrollspy
    $('.scrollspy').scrollSpy();
   //Tooltips
   $('.tooltipped').tooltip({delay: 50});

  $('.parallax').parallax();

});
