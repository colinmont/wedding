class NewUserMailer < ApplicationMailer
  default from: 'Colin & Sarah'

  def welcome_email(user)
    @user = user
    if @user.attending == "Yes"
      @title = "Confirmation!"
      @message = "Thanks for letting us know that you're coming!"
      @message1 = "Your RSVP said that you would like #{@user.meal}
                  for the meal. Excellent choice! Our ceremony will
                  begin at 2:30pm at The Shipyard Market followed by
                     the reception at 6:30pm and the dance at 9:00pm"
    else
      @message = "Sorry you can't make it :("
      @message1 = "Thanks for letting us know though. You are still more
                  than welcome to come to the ceremony starting at 2:30pm
                  at The Shipyard Market in Summerside and to the dance
                  after the reception starting at 9:00pm"
    end
    @picture = 'http://s17.postimg.org/xf2vvrtwf/email.jpg'
    @facebook = 'https://www.facebook.com/sarah.mccarville.50?and=CM0nty&pnref=story'
    @phone = '(902) 303-9500'
    @email = 'wedding@colinandsarah.ca'
    mail(to: @user.email, subject: 'Colin & Sarah - Thanks for RSVPing ' + @user.name)
  end

  def notifier(user)
    @user = user
    #@email_sarah = 'sarah.m.mccarville@gmail.com'
    mail(to: 'colinmmontgomery@gmail.com', subject: @user.name + " just RSVP'd")
  end
end
