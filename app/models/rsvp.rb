class Rsvp < ActiveRecord::Base
	validates :name, :selection,  presence: true
	validates :email, presence: true,
                    length: { minimum: 5 }
					#uniqueness: true
	validates :guests, numericality: true
end
