class User < ActiveRecord::Base
  validates :email, :attending, presence: true
  validates :food, :meal, presence: true, if: :chose_attending?
  validates :food_restrictions, presence:true, if: :chose_restrictions?

  def chose_attending?
    attending == "Yes"
  end

  def chose_restrictions?
    food == "Yes"
  end
end
