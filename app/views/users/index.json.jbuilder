json.users @users do |user|
  json.name user.name
  json.email user.email
  json.attending user.attending
  json.meal user.meal
  json.comments user.comments
end
