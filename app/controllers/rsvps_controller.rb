class RsvpsController < ApplicationController
	before_filter :authenticate, :only => [:index]
	layout "rsvp"

	def index
		@rsvp = Rsvp.all
	end

	def new
		@rsvp = Rsvp.new
	end

	def create
		@rsvp = Rsvp.new(rsvp_params)
		if @rsvp.save
			 # Tell the UserMailer to send a welcome email after save
			NewUserMailer.welcome_email(@rsvp).deliver_now
			render 'thanks'
		else
			render 'new'
		end
	end

	def destroy
		@rsvp = Rsvp.find(params[:id])
		@rsvp.destroy
		render 'deleted'
	end

private
	def rsvp_params
		params.require(:rsvp).permit(:name,:email,:selection,:guests,:guest_names,:comments)
	end
end
