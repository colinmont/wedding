class UsersController < ApplicationController
  layout "rsvp"

  def index
	  @user = User.all
  end

  def new
    @user = User.new[user_params]
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    if @user.update_attributes(user_params)
      NewUserMailer.welcome_email(@user).deliver_now
      NewUserMailer.notifier(@user).deliver_now
      respond_to do |format|
        format.html {render :nothing => true}
      end
    else
      format.json render :nothing => true
    end
  end

  def show
      @user = User.find(params[:id])
  end

private
  def user_params
    params.require(:user).permit(:name, :email, :attending, :meal,:food, :food_restrictions, :comments, :not_coming)
  end
end
