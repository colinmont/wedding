class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :attending
      t.string :meal
      t.text :comments

      t.timestamps null: false
    end
  end
end
