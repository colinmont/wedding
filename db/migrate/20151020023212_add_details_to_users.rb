class AddDetailsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :not_coming, :text
    add_column :users, :food_restrictions, :text
  end
end
