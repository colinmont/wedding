class AddColumnToRsvps < ActiveRecord::Migration
  def change
    add_column :rsvps, :comments, :text
    add_column :rsvps, :meals, :string
    add_column :rsvps, :food_restrictions, :text
  end
end
