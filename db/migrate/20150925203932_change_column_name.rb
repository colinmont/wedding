class ChangeColumnName < ActiveRecord::Migration
  def change
    rename_column :rsvps, :food_restrictions, :guest_names
  end
end
