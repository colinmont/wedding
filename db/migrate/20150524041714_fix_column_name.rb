class FixColumnName < ActiveRecord::Migration
  def change
    change_table :rsvps do |t|
    t.rename :meals, :guests
    end
  end
end
