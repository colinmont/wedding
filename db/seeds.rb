# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = User.create([
  {
    name:"",
    email:"",
    attending:"",
    meal:"",
    comments:""
  },
  {
    name:"Sarah Mccarville",
    email:"",
    attending:"",
    meal:"",
    comments:""
  },

  {

    name:"Barry Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Ellen Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Emily Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Willie Mac",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Andrew Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Amanda Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Clarence Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Maxine Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Krista Keough",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Robert Keough",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Montana Keough",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Kirk Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Amanda Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Olivia Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Clair Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Kathy Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Ryan Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Rachel Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Randy Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Shelly Arsenault",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"David Warren",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Linda ?????",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Maureen Mccarville",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Arlene McCormack",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Nanny",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Wanda",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Terry Norring",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Anna ?????",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Danny Norring",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Debbie Norring",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Kenny Norring",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Deneen Norring",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Peter Norring",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Kathy Norring",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Carol-Anne Dickson",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Dave Dickson",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Phylis King",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Brian King",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Stacey Bondt",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Mark Bondt",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Ben King",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Jacklyn Desroche",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Granny",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Colin Montgomery",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Yvette Montgomery",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Cody Montgomery",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Beth Montgomery",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Krista Montgomery",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Jeff Rogers",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Tyler Rogers",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Hailey Rogers",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Jeff Lecky",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Jacqline ????",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Adam Lecky",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Donnie Montgomery",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Paul Montgomery",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Grace Montgomery",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"John Montgomery",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Janice Montgomery",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Anne Phillips",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"John Phillips",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Jane Little",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Terry Little",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Nanny",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Meme",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Dawn Moase",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"David Moase",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Mary-Ellen Gallant",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Kevin Gallant",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Dean Sharkey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Rena Sharkey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Brian Sharkey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Edward Sharkey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Kelly Sharkey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Diane Sharkey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Travis Sharkey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Blair Sharkey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Judy Sharkey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Norine Sharkey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Rob ????",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Rebecca Griffin",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Rodney MacFadyen",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Katelyn Kelly",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Nathan Dawson",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Kelsey Leard",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"John Boy",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Lyle MacDougal",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Josh Harvey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Megan Harvey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Cole Noonan ",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Erika Jeffery",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Marshall Dawson",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Spencer Leard",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Tiffany Stewart",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Cyle Wheeldon",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Megan Wheeldon",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Janet Reimer",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Curtis Reimer",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Sara Paynter",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Mallory McMurrer",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Chad Murphy",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Andy Murphy",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Pat Paynter",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Jean Clark",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Walter Clark",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Kathy McCardle",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Albert McCardle",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Sherry Webster",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Troy Webster",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Ricky Harvey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Jenna Harvey",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Vance Griffin ",

    email:"",

    attending:"",

    meal:"",

    comments:""

  },

  {

    name:"Annette Griffin ",

    email:"",

    attending:"",

    meal:"",

    comments:""

  }
]
)
